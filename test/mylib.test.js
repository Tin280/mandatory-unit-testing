const chai = require('chai');
const expect = chai.expect;
const mylib = require('../mylib');

/**
 * Import necessary testing libraries and the module to be tested.
 */

describe('Mylib', () => {
  before(() => {
    console.log('Start Testing.');
  });

  after(() => {
    console.log('Finish Test.');
  });
describe('Test caculation funtions', () => {
    /**
     * Test case for the add function.
     */
  it('should add two numbers', () => {
    expect(mylib.add(5, 3)).to.equal(8);
  });

    /**
     * Test case for the subtract function.
     */
  it('should subtract two numbers', () => {
    expect(mylib.subtract(10, 4)).to.equal(6);
  });

    /**
     * Test case for the multiply function.
     */
  it('should multiply two numbers', () => {
    expect(mylib.multiply(6, 2)).to.equal(12);
  });

    /**
     * Test case for the divide function.
     */
  it('should divide two numbers', () => {
    expect(mylib.divide(8, 2)).to.equal(4);
  });
  
    /**
     * Test case for handling division by zero.
     * Expects an error to be thrown.
     */
  it('should throw an error when dividing by zero', () => {
    expect(() => mylib.divide(10, 0)).to.throw('ZeroDivision: Division by zero is not allowed.');
});

})

});