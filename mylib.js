  /**
   * Adds two numbers.
   * @param {number} a - The first number.
   * @param {number} b - The second number.
   * @returns {number} The sum of the two numbers.
  */
  const add = function (a, b) {
    return a + b;
  }

  /**
   * Subtracts one number from another.
   * @param {number} a - The number to subtract from (minuend).
   * @param {number} b - The number to subtract (subtrahend).
   * @returns {number} The result of the subtraction.
   */

  const subtract = function (a, b) {
    return a - b;
  }

  /**
   * Multiplies two numbers.
   * @param {number} a - The first number.
   * @param {number} b - The second number.
   * @returns {number} The product of the two numbers.
   */
  const multiply = function (a, b) {
    return a * b;
  }

    /**
   * Divides one number by another.
   * @param {number} a - The numerator.
   * @param {number} b - The denominator (must not be zero).
   * @throws {Error} Throws an error if b is zero.
   * @returns {number} The result of the division.
   */

  const divide = function (a, b) {
    if (b === 0) {
      throw new Error("ZeroDivision: Division by zero is not allowed.");
    }
    return a / b;
  }


module.exports = {
  add,subtract,multiply,divide
  };