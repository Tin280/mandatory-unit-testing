const mylib = require('./mylib');

const result = {
  /**
   * Perform addition operation.
   * @type {number}
   */
  addition: mylib.add(5, 3),

  /**
   * Perform subtraction operation.
   * @type {number}
   */
  subtraction: mylib.subtract(8, 5),

  /**
   * Perform multiplication operation.
   * @type {number}
   */
  multiplication: mylib.multiply(3, 2),

  /**
   * Perform division operation.
   * @type {number}
   */
  division: mylib.divide(8, 4),
};

console.log(result);